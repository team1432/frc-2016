"""
This is a autonomous module for driving forwards, and turning right
"""

from components import drive
from robotpy_ext.autonomous import timed_state, StatefulAutonomous


class ForwardsAndTurn(StatefulAutonomous):
    """
    Drives forwards and turns right
    """

    MODE_NAME = "Forwards and Right"
    DEFAULT = True

    drive = drive.Drive

    @timed_state(duration=1.2, first=True, next_state='turn')
    def forwards(self):
        self.drive.forwards_at(0.5)

    @timed_state(duration=1.2)
    def turn(self):
        self.drive.turn_at(0.6)
