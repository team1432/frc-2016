import wpilib


class Drive:
    """
        The sole interaction between the robot and its driving system
        occurs here. Anything that wants to drive the robot must go
        through this class.
    """

    robot_drive = wpilib.RobotDrive

    left_drive_motor = wpilib.Talon
    right_drive_motor = wpilib.Talon

    rotation_amount = 0
    forward_amount = 0
    squared_inputs = True

    def forwards_at(self, speed):
        """Causes the robot to drive forwards at the given speed"""
        self.forward_amount = speed

    def turn_at(self, rotation_speed):
        """Causes the robot to turn at the given speed"""
        self.rotation_amount = rotation_speed

    def execute(self):
        """Actually makes the robot drive"""

        self.robot_drive.arcadeDrive(
            self.forward_amount,
            -self.rotation_amount,
            self.squared_inputs)

        self.forward_amount = 0
        self.rotation_amount = 0
