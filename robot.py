#!/usr/bin/env python3
"""
    Robot is the main file for robot code. Here, objects are instantiated,
    and joystick interaction happens.
"""

import magicbot

import wpilib
from components import drive


class Tim(magicbot.MagicRobot):
    """
    Robot class
    """

    drive = drive.Drive

    def createObjects(self):
        self.left_drive_motor = wpilib.Talon(0)
        self.right_drive_motor = wpilib.Talon(1)

        self.robot_drive = wpilib.RobotDrive(
            self.left_drive_motor, self.right_drive_motor)

        self.driver_stick = wpilib.Joystick(0)

    def autonomous(self):
        """Autonomous Setup"""
        print('auto')
        magicbot.MagicRobot.autonomous(self)

    def teleopPeriodic(self):
        try:
            self.drive.forwards_at(-self.driver_stick.getY())
            self.drive.turn_at(self.driver_stick.getX())
        except:
            print('lkjadsfljda')


# Start running if called directly
if __name__ == '__main__':
    wpilib.run(Tim)
